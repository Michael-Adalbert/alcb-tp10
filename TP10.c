#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/list.h>
#include <linux/sched.h>
#include <linux/mutex.h>
#include <linux/ioctl.h>
#include <linux/version.h>


#define LICENCE "GPL"
#define AUTEUR "Michael Adalbert michael.adalbert@univ-tlse3.fr"
#define DESCRIPTION "Exemple de module Master CAMSI"
#define DEVICE "device_tp6"

#define MIN(a, b) ((a) < (b)) ? (a) : (b)

static int my_open(struct inode *i, struct file *f);
static int my_release(struct inode *i, struct file *f);
static ssize_t my_read(struct file *f, char *buf, size_t s, loff_t *t);
static ssize_t my_write(struct file *f, const char *buf, size_t s, loff_t *t);

static dev_t dev;

static struct cdev *my_cdev;

static struct file_operations my_fops = {
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_release,
    .write = my_write,
    .read = my_read
};

static struct class *cl = NULL;

static ssize_t my_read(struct file *f, char *buf, size_t s, loff_t *t)
{
    printk(KERN_ALERT "[ Driver action ] read\n");
    return 0;
}

static ssize_t my_write(struct file *f, const char *buf, size_t s, loff_t *t)
{
    printk(KERN_ALERT "[ Driver action ] write\n");
    return s;
}

static int my_open(struct inode *i, struct file *f)
{
    printk(KERN_ALERT "[ Driver action ] open\n");
    return 0;
}

static int my_release(struct inode *i, struct file *f)
{
    printk(KERN_ALERT "[ Driver action ] close\n");
    return 0;
}

static int buff_init(void)
{
    if (alloc_chrdev_region(&dev, 0, 1, DEVICE) == -1)
    {
        printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
        return -EINVAL;
    }
    printk(KERN_ALERT "[ Init allocated ] (major, minor)=(%d,%d)\n", MAJOR(dev), MINOR(dev));
    printk(KERN_ALERT "[ Driver State ] starting\n");

    my_cdev = cdev_alloc();
    my_cdev->ops = &my_fops;
    my_cdev->owner = THIS_MODULE;

    cl = class_create(THIS_MODULE, "chrdev");

    if (cl == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Class creation failed\n");
        unregister_chrdev_region(dev, 1);
        return -EINVAL;
    }

    if (device_create(cl, NULL, MKDEV(MAJOR(dev), MINOR(dev) ), NULL, "sample-rw") == NULL)
    {
        printk(KERN_ALERT ">>> ERROR : Device creation failed\n");
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 1);
        return -EINVAL;
    }

    if (cdev_add(my_cdev, MKDEV(MAJOR(dev), 0), 1) <= -1)
    {
        device_destroy(cl, dev);
        class_destroy(cl);
        unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 1);
        return -EINVAL;
    }
    
    // Regle udev
    //KERNEL=="sample-rw", SYMLINK+="myDevice", MODE="0666"

    return 0;
}

static void buff_cleanup(void)
{
    printk(KERN_ALERT "[ Driver State ] stopping\n");
    unregister_chrdev_region(MKDEV(MAJOR(dev), 0), 1);
    device_destroy(cl, MKDEV(MAJOR(dev), MINOR(dev)));
    cdev_del(my_cdev);
    class_destroy(cl);
}

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTEUR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);

module_init(buff_init);
module_exit(buff_cleanup);
