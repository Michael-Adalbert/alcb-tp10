#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <linux/ioctl.h>

int main(int argc, char **argv)
{
    int f;
    if((f = open("/dev/myDevice", O_RDWR)) == -1){
        printf("cannot open the file\n");
        return EXIT_FAILURE;
    }
    write(f,NULL,0);
    read(f,NULL,0);
    close(f);
    if((f = open("/dev/sample-rw", O_RDWR)) == -1){
        printf("cannot open the file\n");
        return EXIT_FAILURE;
    }
    write(f,NULL,0);
    read(f,NULL,0);
    close(f);
    return EXIT_SUCCESS;
}